/*
 * Character device driver module.
 * 
 * Common
 *
 * Jacek Łysiak
 */

#ifndef _COMMON_H_
#define _COMMON_H_

/* Put all used headers here... */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/ioctl.h>
#include <linux/ctype.h>        // isdigit
#include <linux/cred.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include "chr_ioctl.h"

#define CHR_TAG                 "[CharDevice] "

#define CHR_MAX_REPS            0x100
#define CHR_DEF_REPS            1

#define CHR_DEV_NAME            "hello"
#define CHR_DEV_BMINOR          0
#define CHR_DEV_CNT             1 

#define WR_ARG_LEN              32

struct user_conf {
        uid_t uid;
        u32 reps;
        struct list_head user_list;
};

struct chr_dev {
        struct user_conf *users;
        spinlock_t lock;
};

// Define global device context
extern struct chr_dev context;

#endif // _COMMON_H_
