/*
 * Character device driver
 * "Hello world!"
 *
 * Jacek Łysiak
 */

#include "chr_common.h"
#include "chr_fops.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jacek Łysiak");
MODULE_DESCRIPTION("Character device driver");


static dev_t chr_major;
static struct cdev chr_cdev;
static struct device *chr_dev;
static struct class chr_class = {
        .name = CHR_DEV_NAME,
        .owner = THIS_MODULE
};
struct chr_dev context;

static struct file_operations chr_fops = {
        .owner = THIS_MODULE,
        .read = chr_read,
        .open = chr_open,
        .unlocked_ioctl = chr_ioctl,
        .compat_ioctl = chr_ioctl,
        .release = chr_release,
        .write = chr_write
};

static int chr_init(void)
{
        int err;
        struct user_conf *root_conf;

        printk(KERN_WARNING CHR_TAG "Initializing module...");
        root_conf = kmalloc(sizeof(struct user_conf), GFP_KERNEL);
        if (root_conf == NULL) {
                err = -ENOMEM;
                goto err_kmalloc;
        }
        INIT_LIST_HEAD(&root_conf->user_list);
        root_conf->reps = CHR_DEF_REPS;
        root_conf->uid = 0;     // Set UID of root user
        context.users = root_conf;
        spin_lock_init(&context.lock);

        // Allocate device numbers range
        // `major` - dynamicaly, `minors` - a priori
        err = alloc_chrdev_region(&chr_major, CHR_DEV_BMINOR, 
                        CHR_DEV_CNT, CHR_DEV_NAME);
        if (err)
                goto err_alloc;
        // Initialise char device struct, further part of inode
        // Static init -> free memory of chr_cdev after all if needed
        // Here it's just global
        cdev_init(&chr_cdev, &chr_fops);
        // Add one dev
        if ((err = cdev_add(&chr_cdev, chr_major, 1)))
                goto err_cdev;
        // Register class, once for all devices
        if ((err = class_register(&chr_class)))
                goto err_class;
        // Register dev in sysfs
        chr_dev = device_create(&chr_class, NULL, 
                        chr_major, NULL, CHR_DEV_NAME);
        if (IS_ERR(chr_dev)) {
                err = PTR_ERR(chr_dev);
                goto err_dev;
        }

        return 0;

err_dev:
        class_unregister(&chr_class);
err_class:
        cdev_del(&chr_cdev);
err_cdev:
        unregister_chrdev_region(chr_major, CHR_DEV_CNT);
err_alloc:
        kfree(root_conf);
err_kmalloc:
        return err;
}

static void chr_exit(void)
{
        struct user_conf *tmp, *n; 
        printk(KERN_WARNING CHR_TAG "Unloading module...");
        spin_lock(&context.lock); // Is it required??
        list_for_each_entry_safe(tmp, n, &context.users->user_list, user_list) {
                list_del(&tmp->user_list);
                kfree(tmp);
        }
        spin_unlock(&context.lock);
        
        device_destroy(&chr_class, chr_major);
        class_unregister(&chr_class);
        cdev_del(&chr_cdev);
        unregister_chrdev_region(chr_major, CHR_DEV_CNT);
}


module_init(chr_init);
module_exit(chr_exit);
