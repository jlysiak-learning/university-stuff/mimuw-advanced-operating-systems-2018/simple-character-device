/*
 * Character device driver module.
 * 
 * File operations
 *
 * Jacek Łysiak
 */

#ifndef _FOPS_H_
#define _FOPS_H_

#include "chr_common.h"

ssize_t chr_read(struct file *, char __user *, size_t, loff_t *);
ssize_t chr_write(struct file *, const char __user *, size_t, loff_t *);

long chr_ioctl(struct file *, unsigned int, unsigned long);
int chr_open(struct inode *, struct file *);
int chr_release(struct inode *, struct file *);

#endif // _FOPS_H_
