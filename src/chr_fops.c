/*
 * File operations.
 *
 * Jacek Łysiak
 */

#include "chr_common.h"

static const char msg[] = "Hello, World!\n";
static size_t msg_len = sizeof(msg) - 1;

struct user_conf * find_user_conf(void)
{
        uid_t uid;
        struct user_conf *tmp;
        struct user_conf *result;
        uid = current_uid().val;

        printk(KERN_INFO CHR_TAG "Find user conf by uid=%d\n", uid);

        result = NULL;
        spin_lock(&context.lock);
        list_for_each_entry(tmp, &context.users->user_list, user_list) {
                printk(KERN_INFO CHR_TAG "moving along list: uid=%d, reps=%d\n", tmp->uid, tmp->reps);
                if (tmp->uid == uid) {
                        result = tmp;
                        printk(KERN_INFO CHR_TAG "Found!\n");
                        break;
                }
        }
        spin_unlock(&context.lock);
        return result;
}

struct user_conf * add_user_conf(void)
{
        struct user_conf *conf;
        printk(KERN_INFO CHR_TAG "Adding user conf struct...");
        conf = kmalloc(sizeof(struct user_conf), GFP_KERNEL);
        if (conf == NULL)
                return NULL;
        conf->uid = current_uid().val;
        conf->reps = CHR_DEF_REPS;
        spin_lock(&context.lock);
        list_add_tail(&conf->user_list, &context.users->user_list);
        spin_unlock(&context.lock);
        return conf;         
}

ssize_t chr_read(struct file *filep, char __user *buf, size_t count, loff_t *filepos)
{
        loff_t pos, end;
        size_t f_len;
        int reps;
        printk(KERN_INFO CHR_TAG "Reading...\n");
        reps = ((struct user_conf *) filep->private_data)->reps;
        f_len = msg_len * reps;
        pos = *filepos;
        if (pos >= f_len || pos < 0)
                return 0;
        if (count > f_len - pos)
                count = f_len - pos;
        end = pos + count;
        while (pos < end)
                if (put_user(msg[pos++ % msg_len], buf++))
                        return -EFAULT;
        *filepos = pos;
        return count;
}

ssize_t chr_write(struct file *filep, const char __user *buf, size_t count, loff_t *filepos)
{
        char str[WR_ARG_LEN + 1];
        char last;
        int val;
        printk(KERN_INFO CHR_TAG "Calling write...\n");
        if (count > WR_ARG_LEN || count == 0)
                return -EINVAL;
        if (copy_from_user(str, buf, count))
                return -EFAULT; // ALWAYS on error
        str[count] = '\0';
        for (int i = 0; i < count - 1; i++)
                if (!isdigit(str[i]))
                        return -EINVAL;
        last = str[count - 1];
        if (!isdigit(last)) {
                if (last != '\n')
                        return -EINVAL;
                if (count == 1)
                        return -EINVAL;
        }
        sscanf(str, "%d", &val);
        printk(KERN_INFO CHR_TAG "Written: %d\n", val);
        if (val > CHR_MAX_REPS)
                return -EINVAL;
        *filepos += count;
        ((struct user_conf *) filep->private_data)->reps = val;
        return count;
}

long chr_ioctl(struct file *filep, unsigned int cmd, unsigned long arg)
{
        printk(KERN_INFO CHR_TAG "Calling ioctl: cmd=%u, arg=%lu\n", cmd, arg);
        if (cmd != CHR_IOCTL_SET_REPS)
                return -ENOTTY;
        if (arg > CHR_MAX_REPS)
                return -EINVAL;
        ((struct user_conf *) filep->private_data)->reps = arg;
        return 0;
}

int chr_open(struct inode *inode, struct file *filep)
{
        struct user_conf *conf;
        printk(KERN_INFO CHR_TAG "Opening by uid=%d\n", current_uid().val);
        conf = find_user_conf();
        if (conf == NULL)
                conf = add_user_conf();
        if (conf == NULL)
                return -ENOMEM; // kmalloc failed
        filep->private_data = (void *) conf;
        return 0;
}

int chr_release(struct inode *inode, struct file *filep)
{
        printk(KERN_WARNING CHR_TAG "Releasing...\n");
        return 0;
}

