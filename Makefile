KDIR ?= /home/jacek/Workspace/zso-2017/hshare/lib/modules/4.9.13/build

default:
	$(MAKE) -C $(KDIR) M=$$PWD/src
	cp src/chrdev.ko ~/Workspace/zso-2017/hshare/ 

install:
	$(MAKE) -C $(KDIR) M=$$PWD/src modules_install

clean:
	$(MAKE) -C $(KDIR) M=$$PWD/src clean

